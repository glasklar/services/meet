This repository collects user instructions and tips & tricks, and is used for tracking operational issues with https://meet.glasklar.is/

We are using Jitsi. Seems to be pretty OK, but it happens that the audio in particular does not work for all participants.
Please open an issue, or find an appropriate issue to add information to, when Jitsi doesn't work as expected. Make sure to include information about what client you're using -- FF on Linux, Chrome on Windows, Android/iPhone app, other.
