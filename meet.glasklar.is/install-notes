2024-08-05

jitsi-01.glasklarteknik.se gets its base configuration from ansible.  The actual
jitsi software was however installed manually using Jitsi's quickstart guide:

    http://web.archive.org/web/20240805055407/https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart/

This is similar to the previous installation of meet.sigsum.org, see:

    https://git.glasklar.is/glasklar/services/misc/-/blob/main/hosts/meet.sigsum.org.txt

---INSTALL NOTES

apt update
apt install gnupg2 nginx-full curl wget apt-transport-https lua5.2

curl https://download.jitsi.org/jitsi-key.gpg.key | sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
curl -sL https://prosody.im/files/prosody-debian-packages.key -o /etc/apt/keyrings/prosody-debian-packages.key
echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null
echo "deb [signed-by=/etc/apt/keyrings/prosody-debian-packages.key] http://packages.prosody.im/debian $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/prosody-debian-packages.list
apt update

mkdir /etc/nftables.conf.d
cat <<EOF >/etc/nftables.conf.d/jitsi.conf
table inet filter {
        chain local_inp {
                tcp dport 80 accept comment http-web       # For SSL certificate verification / renewal with Let's Encrypt
                tcp dport 443 accept comment https-web     # For general access to Jitsi Meet
                udp dport 10000 accept comment video-voice # For General Network Audio/Video Meetings
                udp dport 3478 accept comment stun         # For querying the stun server
                tcp dport 5349 accept comment fallback     # For fallback network video/audio communications over TCP
        }
}
EOF
systemctl reload nftables

apt install jitsi-meet
- Configuring jitsi-videobridge2
  - meet.glasklar.is
- Configuring jitsi-meet-web-config
  - Let's Encrypt certificates
- Configuring jitsi-meet-web-config
  - registry@glasklarteknik.se
- Configuring jitsi-meet-web-config
  - No (if we want dial-in we can look into that separately)

---Some privacy in nginx's access log
vim /etc/nginx/nginx.conf
http {
...
        map $remote_addr $remote_addr_anon {
            ~(?P<ip>\d+\.\d+)\.    $ip.0.0;
            ~(?P<ip>[^:]+:[^:]+):  $ip::;
            default                0.0.0.0;
        }
        log_format srcaddrprivacy '$remote_addr_anon  - $remote_user [$time_local] '
                    '"$request" $status $body_bytes_sent '
                    '"$http_referer" "$http_user_agent"';

        access_log /var/log/nginx/access.log srcaddrprivacy;
...
}

---A note on TLS certificates
The above jitsi install took care of aquiring and renewing certificates.

crontab -l
openssl x509 -in /etc/jitsi/meet/meet.glasklar.is.crt -text -noout
openssl x509 -in /etc/prosody/certs/meet.glasklar.is.crt -text -noout

---A note on debugging
Some possibly useful files:

/var/log/jitsi/jvb.log
/var/log/jitsi/jicofo.log
/var/log/prosody/prosody.log
/var/log/nginx/access.log
/var/log/nginx/error.log
