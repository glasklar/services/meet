Timeline of changes to and issues with the Jitsi service at https://meet.sigsum.org/.

| date       | event                                          | notes          | reference                                                 |
|------------|------------------------------------------------|----------------|-----------------------------------------------------------|
| 2023-01-31 | software upgrades                              |                | https://git.glasklar.is/glasklar/services/meet/-/issues/3 |
| 2023-02-02 | user dropping, unable to reconnect immediately | client: Safari |                                                           |
| 2023-02-24 | software upgrades                              |                |                                                           |
| 2024-08-07 | planned shutdown/deprecation                   | use meet.gis   |                                                           |
